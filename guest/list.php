<?php
require_once __DIR__ . '/../boot.php';

$page_path = "/guest/index.php";

$type_id = get('type');

if ($type_id) {
    $sql = "SELECT * FROM `polls` 
    INNER JOIN `users` ON `users`.`user_id`=`polls`.`user_id`
    LEFT JOIN `poll_types` ON `poll_types`.`poll_type_id`=`polls`.`poll_type_id`
    WHERE `polls`.`poll_type_id`='{$type_id}'";
} else {
    $sql = "SELECT * FROM `polls` 
    INNER JOIN `users` ON `users`.`user_id`=`polls`.`user_id`
    LEFT JOIN `poll_types` ON `poll_types`.`poll_type_id`=`polls`.`poll_type_id`";
}

$types = DB::result("SELECT * FROM `poll_types`");
$items = DB::result($sql);

ob_start();
?>
<?= showAlert() ?>
<form method="get">
    <label for="type">เลือกประเภทแบบสำรวจ</label>
    <select name="type" id="type">
        <option value="">ทั้งหมด</option>
        <?php foreach ($types as $item) : ?>
            <option value="<?= $item['poll_type_id'] ?>" <?= $type_id === $item['poll_type_id'] ? 'selected' : null ?>><?= $item['poll_type_name'] ?></option>
        <?php endforeach; ?>
    </select>
    <button type="submit">ค้นหา</button>
</form>

<table>
    <thead>
        <th>รหัส</th>
        <th>ชื่อแบบสำรวจ</th>
        <th>ชื่อประเภทแบบสำรวจ</th>
        <th>ผู้สร้างแบบสำรวจ</th>
        <th></th>
    </thead>
    <tbody>
        <?php foreach ($items as $item) : ?>
            <tr>
                <td><?= $item['poll_id'] ?></td>
                <td><?= $item['poll_name'] ?></td>
                <td><?= $item['poll_type_name'] ?></td>
                <td><?= $item['firstname'] . ' ' . $item['lastname'] ?></td>
                <td>
                    <a href="<?= url("/guest/poll.php?id={$item['poll_id']}") ?>" target="_blank" rel="noopener noreferrer">เปิดแบบสำรวจ</a>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<?php
$layout_page = ob_get_clean();
$page_name = 'รายการแบบสำรวจ';
require ROOT . '/guest/layout.php';
