<?php
ob_start();
?>
<h1><?= conf('app_name') ?></h1>
<hr>

<h1><?= isset($page_name) ? $page_name : null ?></h1>

<nav>
    <h3>เมนู</h3>
    <ul>
        <li><a href="<?= url('/guest/index.php') ?>">หน้าแรก</a></li>
        <li><a href="<?= url('/guest/list.php') ?>">รายการแบบสำรวจ</a></li>
        <li><a href="<?= url('/guest/search.php') ?>">ค้นหาแบบสำรวจ</a></li>
        <li>
            <ul>
                <li><a href="<?= url('/auth/login.php') ?>">เข้าสู่ระบบ</a></li>
                <li><a href="<?= url('/auth/register.php') ?>">สมัครสมาชิก</a></li>
            </ul>
        </li>
    </ul>
</nav>

<main>
    <?= isset($layout_page) ? $layout_page : null ?>
</main>
<?php
$layout_body = ob_get_clean();
require INC . '/base_layout.php'; 
