<?php
require_once __DIR__ . '/../boot.php';

$page_path = "/guest/index.php";

$search = get('search');

$sql = "SELECT * FROM `polls` 
INNER JOIN `users` ON `users`.`user_id`=`polls`.`user_id`
LEFT JOIN `poll_types` ON `poll_types`.`poll_type_id`=`polls`.`poll_type_id`
WHERE `polls`.`poll_name` LIKE '%{$search}%'";


$types = DB::result("SELECT * FROM `poll_types`");
$items = DB::result($sql);

ob_start();
?>
<?= showAlert() ?>
<form method="get">
    <label for="type">ค้นหาแบบสำรวจ</label>
    <input type="search" name="search" id="search" value="<?= $search ?>">
    <button type="submit">ค้นหา</button>
</form>

<table>
    <thead>
        <th>รหัส</th>
        <th>ชื่อแบบสำรวจ</th>
        <th>ชื่อประเภทแบบสำรวจ</th>
        <th>ผู้สร้างแบบสำรวจ</th>
        <th></th>
    </thead>
    <tbody>
        <?php foreach ($items as $item) : ?>
            <tr>
                <td><?= $item['poll_id'] ?></td>
                <td><?= $item['poll_name'] ?></td>
                <td><?= $item['poll_type_name'] ?></td>
                <td><?= $item['firstname'] . ' ' . $item['lastname'] ?></td>
                <td>
                    <a href="<?= url("/guest/poll.php?id={$item['poll_id']}") ?>" target="_blank" rel="noopener noreferrer">เปิดแบบสำรวจ</a>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<?php
$layout_page = ob_get_clean();
$page_name = 'ค้นหาแบบสำรวจ';
require ROOT . '/guest/layout.php';
