<?php
require_once __DIR__ . '/../boot.php';

$poll_id = get('id');
$page_path = "/guest/poll.php?id=" . $poll_id;

if (post('q')) {
    $poll_action_re = DB::insert('poll_action', [
        'poll_id' => $poll_id,
        'created_at' => date(DATE_SQL)
    ]);

    if (!$poll_action_re) {
        setAlert('error', 'เกิดข้อผิดพลาด ไม่สามารถส่งแบบสำรวจได้');
        redirect($page_path);
    }

    $poll_action_id = DB::$conn->insert_id;
    $items_data = [];
    $q = post('q');
    foreach ($q as $key => $value) {
        $items_data[] = [
            'poll_action_id' => $poll_action_id,
            'q_id' => $key,
            'ans_id' => $value
        ];
    }
    
    $ans_re = DB::insert_multi('poll_action_items', $items_data);
    if ($ans_re) {
        setAlert('success', "ส่งแบบสำรวจสำเร็จเรียบร้อย");
    } else {
        setAlert('error', "เกิดข้อผิดพลาด ไม่สามารถส่งคำตอบได้");
    }

    redirect($page_path);
}

$data = DB::row("SELECT * FROM `polls` 
INNER JOIN `users` ON `users`.`user_id`=`polls`.`user_id`
LEFT JOIN `poll_types` ON `poll_types`.`poll_type_id`=`polls`.`poll_type_id`
WHERE `polls`.`poll_id`='{$poll_id}'");

$items = DB::result("SELECT * FROM `questions` WHERE `poll_id`='{$poll_id}'");
foreach ($items as &$item) {
    $item['answers'] = DB::result("SELECT * FROM `answers` WHERE `q_id`='{$item['q_id']}'");
    unset($item);
}

ob_start();
?>
<h1><?= $data['poll_name'] ?></h1>
<p>
    ประเภทแบบสำรวจ: <?= $data['poll_type_name'] ?>
    <br>
    สร้างโดย: <?= $data['firstname'] . $data['lastname'] ?>
</p>

<?= showAlert() ?>
<form method="post">
    <?php foreach ($items as $item) : ?>
        <h3><?= $item['q_name'] ?></h3>
        <ul>
            <?php foreach ($item['answers'] as $ans) : ?>
                <input type="radio" name="q[<?= $item['q_id'] ?>]" id="ans<?= $ans['ans_id'] ?>" value="<?= $ans['ans_id'] ?>">
                <label for="ans<?= $ans['ans_id'] ?>"><?= $ans['ans_name'] ?></label>
                <br>
            <?php endforeach; ?>
        </ul>
        <br>
    <?php endforeach; ?>
    <button type="submit">ส่งแบบสำรวจ</button>
</form>

<?php
$layout_page = ob_get_clean();
$page_name = 'แบบสำรวจ';
require ROOT . '/guest/layout.php';
