-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Jan 17, 2023 at 11:55 AM
-- Server version: 5.7.34
-- PHP Version: 7.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `skill65_poll2`
--

-- --------------------------------------------------------

--
-- Table structure for table `answers`
--

DROP TABLE IF EXISTS `answers`;
CREATE TABLE `answers` (
  `ans_id` int(11) NOT NULL,
  `q_id` int(11) NOT NULL,
  `ans_name` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `answers`
--

INSERT INTO `answers` (`ans_id`, `q_id`, `ans_name`) VALUES
(1, 1, '1'),
(2, 1, '2'),
(3, 1, '3'),
(6, 1, '4'),
(10, 3, '1'),
(11, 3, '2'),
(12, 3, '3'),
(13, 3, '4'),
(14, 4, '1'),
(15, 4, '2'),
(16, 4, '3'),
(17, 4, '4'),
(18, 6, '80'),
(19, 6, '70'),
(20, 6, '75'),
(21, 6, '98'),
(22, 5, '3'),
(23, 5, '5'),
(24, 5, '8'),
(25, 5, '2');

-- --------------------------------------------------------

--
-- Table structure for table `polls`
--

DROP TABLE IF EXISTS `polls`;
CREATE TABLE `polls` (
  `poll_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `poll_type_id` int(11) NOT NULL,
  `poll_name` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `polls`
--

INSERT INTO `polls` (`poll_id`, `user_id`, `poll_type_id`, `poll_name`) VALUES
(2, 6, 4, 'asdf'),
(3, 6, 1, 'nnn');

-- --------------------------------------------------------

--
-- Table structure for table `poll_action`
--

DROP TABLE IF EXISTS `poll_action`;
CREATE TABLE `poll_action` (
  `poll_action_id` int(11) NOT NULL,
  `poll_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `poll_action`
--

INSERT INTO `poll_action` (`poll_action_id`, `poll_id`, `created_at`) VALUES
(1, 3, '2023-01-16 16:01:58'),
(2, 3, '2023-01-16 16:01:09'),
(3, 3, '2023-01-16 16:01:13'),
(4, 3, '2023-01-16 16:01:28'),
(5, 2, '2023-01-16 16:01:39'),
(6, 2, '2023-01-16 16:01:39'),
(7, 2, '2023-01-16 16:01:25');

-- --------------------------------------------------------

--
-- Table structure for table `poll_action_items`
--

DROP TABLE IF EXISTS `poll_action_items`;
CREATE TABLE `poll_action_items` (
  `poll_action_item_id` int(11) NOT NULL,
  `poll_action_id` int(11) NOT NULL,
  `q_id` int(11) NOT NULL,
  `ans_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `poll_action_items`
--

INSERT INTO `poll_action_items` (`poll_action_item_id`, `poll_action_id`, `q_id`, `ans_id`) VALUES
(1, 1, 3, 13),
(2, 2, 1, 6),
(3, 2, 3, 11),
(4, 3, 1, 2),
(5, 3, 3, 10),
(6, 4, 1, 2),
(7, 4, 3, 11),
(8, 5, 4, 16),
(9, 5, 6, 20),
(10, 6, 4, 17),
(11, 6, 6, 19),
(12, 7, 4, 17),
(13, 7, 5, 23);

-- --------------------------------------------------------

--
-- Table structure for table `poll_types`
--

DROP TABLE IF EXISTS `poll_types`;
CREATE TABLE `poll_types` (
  `poll_type_id` int(11) NOT NULL,
  `poll_type_name` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `poll_types`
--

INSERT INTO `poll_types` (`poll_type_id`, `poll_type_name`) VALUES
(1, 'asdfg'),
(4, 'hjb'),
(5, 'qwertyuiop'),
(6, 'bbbb'),
(7, 'zxcv');

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

DROP TABLE IF EXISTS `questions`;
CREATE TABLE `questions` (
  `q_id` int(11) NOT NULL,
  `poll_id` int(11) NOT NULL,
  `q_name` varchar(60) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`q_id`, `poll_id`, `q_name`) VALUES
(1, 3, '1+4'),
(3, 3, '1+1'),
(4, 2, '2+2'),
(5, 2, '1+2'),
(6, 2, '20+50=');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `firstname` varchar(25) NOT NULL,
  `lastname` varchar(25) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(65) NOT NULL,
  `user_type` enum('admin','user') NOT NULL,
  `status` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `firstname`, `lastname`, `email`, `password`, `user_type`, `status`) VALUES
(5, 'admin', 'demo', 'admin@demo.com', '25d55ad283aa400af464c76d713c07ad', 'admin', 1),
(6, 'user', 'demo', 'user@demo.com', '25d55ad283aa400af464c76d713c07ad', 'user', 1),
(9, 'user2', 'demo', 'user2@demo.com', '25d55ad283aa400af464c76d713c07ad', 'user', -1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `answers`
--
ALTER TABLE `answers`
  ADD PRIMARY KEY (`ans_id`);

--
-- Indexes for table `polls`
--
ALTER TABLE `polls`
  ADD PRIMARY KEY (`poll_id`);

--
-- Indexes for table `poll_action`
--
ALTER TABLE `poll_action`
  ADD PRIMARY KEY (`poll_action_id`);

--
-- Indexes for table `poll_action_items`
--
ALTER TABLE `poll_action_items`
  ADD PRIMARY KEY (`poll_action_item_id`);

--
-- Indexes for table `poll_types`
--
ALTER TABLE `poll_types`
  ADD PRIMARY KEY (`poll_type_id`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`q_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `answers`
--
ALTER TABLE `answers`
  MODIFY `ans_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `polls`
--
ALTER TABLE `polls`
  MODIFY `poll_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `poll_action`
--
ALTER TABLE `poll_action`
  MODIFY `poll_action_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `poll_action_items`
--
ALTER TABLE `poll_action_items`
  MODIFY `poll_action_item_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `poll_types`
--
ALTER TABLE `poll_types`
  MODIFY `poll_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `q_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
