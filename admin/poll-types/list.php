<?php
require_once __DIR__ . '/../../boot.php';
checkAuth('admin');

$page_path = "/admin/poll-types/list.php";

$action = get('action');
$id = get('id');
$sql = null;

switch ($action) {
    case 'delete':
        DB::delete('poll_types', "`poll_type_id`='{$id}'");
        break;
}

if (isset($action)) {
    redirect($page_path);
}

if ($_POST) {
    $qr = DB::insert('poll_types', [
        'poll_type_name' => post('poll_type_name')
    ]);

    if ($qr) {
        setAlert('success', "เพิ่มเภทแบบสํารวจสำเร็จเรียบร้อย");
    } else {
        setAlert('error', "เกิดข้อผิดพลาด ไม่สามารถเพิ่มเภทแบบสํารวจได้");
    }

    redirect($page_path);
}

$items = DB::result("SELECT * FROM `poll_types`");

ob_start();
?>
<?= showAlert() ?>
<h3>เพิ่มเภทแบบสํารวจ</h3>
<form method="post">
    <label for="poll_type_name">ชื่อเภทแบบสํารวจ</label>
    <input type="text" name="poll_type_name" id="poll_type_name" required>
    <button type="submit">เพิ่ม</button>
</form>

<h3>รายการเภทแบบสํารวจ</h3>
<table>
    <thead>
        <th>รหัส</th>
        <th>ชื่อประเภทแบบสํารวจ</th>
        <th>จัดการประเภทแบบสํารวจ</th>
    </thead>
    <tbody>
        <?php foreach ($items as $item) : ?>
            <tr>
                <td><?= $item['poll_type_id'] ?></td>
                <td><?= $item['poll_type_name'] ?></td>
                <td>
                    <a href="<?= url('/admin/poll-types/edit.php') ?>?id=<?= $item['poll_type_id'] ?>">
                        แก้ไข
                    </a>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="?action=delete&id=<?= $item['poll_type_id'] ?>" <?= clickConfirm("คุณต้องการลบประเภทแบบสํารวจ {$item['poll_type_name']} หรือไม่") ?>>
                        ลบ
                    </a>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<?php
$layout_page = ob_get_clean();
$page_name = 'จัดการประเภทแบบสํารวจ';
require ROOT . '/admin/layout.php';
