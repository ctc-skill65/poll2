<?php
require_once __DIR__ . '/../../boot.php';
checkAuth('admin');

$page_path = "/admin/poll-types/edit.php";

$id = get('id');

if ($_POST) {
    $qr = DB::update('poll_types', [
        'poll_type_name' => post('poll_type_name')
    ], "`poll_type_id`='{$id}'");

    if ($qr) {
        setAlert('success', "แก้ไขประเภทแบบสํารวจสำเร็จเรียบร้อย");
    } else {
        setAlert('error', "เกิดข้อผิดพลาด ไม่สามารถแก้ไขประเภทแบบสํารวจได้");
    }

    redirect('/admin/poll-types/list.php');
}

$data = DB::row("SELECT * FROM `poll_types` WHERE `poll_type_id`='{$id}'");

ob_start();
?>
<?= showAlert() ?>
<h3>แก้ไขประเภทแบบสํารวจ</h3>
<form method="post">
    <label for="poll_type_name">ชื่อประเภทแบบสํารวจ</label>
    <input type="text" name="poll_type_name" id="poll_type_name" value="<?= $data['poll_type_name'] ?>" required>
    <button type="submit">บันทึก</button>
</form>

<?php
$layout_page = ob_get_clean();
$page_name = 'จัดการประเภทแบบสํารวจ';
require ROOT . '/admin/layout.php';
