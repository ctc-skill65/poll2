<?php
require_once __DIR__ . '/../../boot.php';
checkAuth('user');

$page_path = "/user/polls/list.php";

$items = DB::result("SELECT `polls`.*,
poll_types.*,
COUNT(`poll_action`.`poll_id`) AS poll_count
FROM `polls`
LEFT JOIN `poll_action` ON `polls`.`poll_id`=`poll_action`.`poll_id`
LEFT JOIN `poll_types` ON `poll_types`.`poll_type_id`=`polls`.`poll_type_id`
WHERE `polls`.`user_id`='{$user_id}'
GROUP BY `polls`.`poll_id`");

foreach ($items as &$poll) {
    $poll['qs'] = DB::result("SELECT `questions` .* ,
    COUNT(`poll_action_items`.`poll_action_item_id`) AS q_count
    FROM `questions` 
    LEFT JOIN `poll_action_items` ON `poll_action_items`.`q_id`=`questions`.`q_id`
    WHERE `questions`.`poll_id`='{$poll['poll_id']}'
    GROUP BY `questions`.`q_id`");

    foreach ($poll['qs'] as &$q) {
        $q['anss'] = DB::result("SELECT `answers`.*,
        COUNT(`poll_action_items`.`ans_id`) as ans_count
        FROM `answers` 
        LEFT JOIN `poll_action_items` ON `poll_action_items`.`ans_id`=`answers`.`ans_id`
        WHERE `answers`.`q_id`='{$q['q_id']}'
        GROUP BY `answers`.`ans_id`");
        unset($q);
    }

    unset($poll);
}

ob_start();
?>
<?= showAlert() ?>

<table>
    <thead>
        <th>รหัส</th>
        <th>ชื่อแบบสำรวจ</th>
        <th>ชื่อประเภทแบบสำรวจ</th>
        <th>จำนวนการตอบแบบสำรวจ</th>
        <th></th>
    </thead>
    <tbody>
        <?php foreach ($items as $item) : ?>
            <tr>
                <td><?= $item['poll_id'] ?></td>
                <td><?= $item['poll_name'] ?></td>
                <td><?= $item['poll_type_name'] ?></td>
                <td><?= $item['poll_count'] ?></td>
                <td>
                    <table>
                        <thead>
                            <tr>
                                <th>คำถาม</th>
                                <th>จำนวนการตอบคำถาม</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($item['qs'] as $q) : ?>
                                <tr>
                                    <td><?= $q['q_name'] ?></td>
                                    <td><?= $q['q_count'] ?></td>
                                    <td>
                                        <table>
                                            <thead>
                                                <tr>
                                                    <th>คำตอบ</th>
                                                    <th>จำนวนที่ตอบ</th>
                                                    <th>ร้อยละของการตอบ</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php foreach ($q['anss'] as $ans) : ?>
                                                <tr>
                                                    <td><?= $ans['ans_name'] ?></td>
                                                    <td><?= $ans['ans_count'] ?></td>
                                                    <td><?= $ans['ans_count'] !== 0 ? round(intval($ans['ans_count']) * 100 / intval($q['q_count']), 2) : 0 ?></td>
                                                </tr>
                                            <?php endforeach; ?>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<?php
$layout_page = ob_get_clean();
$page_name = 'สถิติการตอบแบบสํารวจ';
require ROOT . '/user/layout.php';
