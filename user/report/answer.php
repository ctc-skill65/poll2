<?php
require_once __DIR__ . '/../../boot.php';
checkAuth('user');

$page_path = "/user/polls/list.php";

$items = DB::result("SELECT * FROM `poll_action`
LEFT JOIN `polls` ON `polls`.`poll_id`=`poll_action`.`poll_id`
LEFT JOIN `poll_types` ON `poll_types`.`poll_type_id`=`polls`.`poll_type_id`
WHERE `polls`.`user_id`='{$user_id}'
ORDER BY `poll_action`.`created_at` DESC");

foreach ($items as &$item) {
    $item['action_items'] = DB::result("SELECT * FROM `poll_action_items` 
    LEFT JOIN `questions` ON `questions`.`q_id`=`poll_action_items`.`q_id`
    LEFT JOIN `answers` ON `answers`.`ans_id`=`poll_action_items`.`ans_id`
    WHERE `poll_action_items`.`poll_action_id`='{$item['poll_action_id']}'");
    unset($item);
}

ob_start();
?>
<?= showAlert() ?>

<table>
    <thead>
        <th>รหัส</th>
        <th>ชื่อแบบสำรวจ</th>
        <th>ชื่อประเภทแบบสำรวจ</th>
        <th>ข้อมูลการตอบแบบสำรวจ</th>
        <th>เวลาตอบแบบสำรวจ</th>
    </thead>
    <tbody>
        <?php foreach ($items as $item) : ?>
            <tr>
                <td><?= $item['poll_action_id'] ?></td>
                <td><?= $item['poll_name'] ?></td>
                <td><?= $item['poll_type_name'] ?></td>
                <td>
                    <table>
                        <thead>
                            <tr>
                                <th>คำถาม</th>
                                <th>คำตอบ</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($item['action_items'] as $action) : ?>
                                <tr>
                                    <td><?= $action['q_name'] ?></td>
                                    <td><?= $action['ans_name'] ?></td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </td>
                <td><?= $item['created_at'] ?></td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<?php
$layout_page = ob_get_clean();
$page_name = 'รายงานการตอบแบบสำรวจ';
require ROOT . '/user/layout.php';
