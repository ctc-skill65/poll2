<?php
require_once __DIR__ . '/../../boot.php';
checkAuth('user');


$poll_id = get('poll');
$q_id = get('q');
$page_path = "/user/polls/edit-question.php?poll={$poll_id}&q={$q_id}";

$action = get('action');
$id = get('id');

switch ($action) {
    case 'delete':
        DB::delete('answers', "`ans_id`='{$id}'");
        break;
}

if (isset($action)) {
    redirect($page_path);
}

if (post('q_name')) {
    $qr = DB::update('questions', [
        'q_name' => post('q_name')
    ], "`q_id`='{$q_id}'");

    if ($qr) {
        setAlert('success', "แก้ไขคำถามสำเร็จเรียบร้อย");
    } else {
        setAlert('error', "เกิดข้อผิดพลาด ไม่สามารถแก้ไขคำถามได้");
    }

    redirect($page_path);
} elseif (post('ans_name')) {
    $qr = DB::insert('answers', [
        'q_id' => $q_id,
        'ans_name' => post('ans_name')
    ]);

    if ($qr) {
        setAlert('success', "เพื่อคำตอบสำเร็จเรียบร้อย");
    } else {
        setAlert('error', "เกิดข้อผิดพลาด ไม่สามารถเพื่อคำตอบได้");
    }

    redirect($page_path);
}

$data = DB::row("SELECT * FROM `questions` WHERE `q_id`='{$q_id}'");
$items = DB::result("SELECT * FROM `answers` WHERE `q_id`='{$q_id}'");

ob_start();
?>
<a href="<?= url("/user/polls/edit.php?poll={$poll_id}") ?>">
    <button>
        < กลับ
    </button>
</a>
<?= showAlert() ?>
<h3>แก้ไขคำถาม</h3>
<form method="post">
    <label for="q_name">ชื่อคำถาม</label>
    <input type="text" name="q_name" id="q_name" value="<?= $data['q_name'] ?>" required>
    <br>
    <button type="submit">บันทึก</button>
</form>

<h3>เพื่อคำตอบ</h3>
<form method="post">
    <label for="ans_name">คำตอบ</label>
    <input type="text" name="ans_name" id="ans_name" required>
    <br>
    <button type="submit">บันทึก</button>
</form>

<h3>รายการคำตอบ</h3>
<table>
    <thead>
        <tr>
            <th>รหัส</th>
            <th>คำตอบ</th>
            <th>จัดการคำตอบ</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($items as $item) : ?>
            <tr>
                <td><?= $item['ans_id'] ?></td>
                <td><?= $item['ans_name'] ?></td>
                <td>
                    <a href="<?= url($page_path) ?>&action=delete&id=<?= $item['ans_id'] ?>" <?= clickConfirm("คุณต้องการลบคำตอบ {$item['ans_name']} หรือไม่") ?>>
                        ลบ
                    </a>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<?php
$layout_page = ob_get_clean();
$page_name = 'แก้ไขคำถาม';
require ROOT . '/user/layout.php';
