<?php
require_once __DIR__ . '/../../boot.php';
checkAuth('user');


$poll_id = get('poll');
$page_path = "/user/polls/edit.php?poll={$poll_id}";

$action = get('action');
$id = get('id');

switch ($action) {
    case 'delete':
        DB::delete('questions', "`q_id`='{$id}'");
        break;
}

if (isset($action)) {
    redirect($page_path);
}

if (post('poll_name')) {
    $qr = DB::update('polls', [
        'poll_name' => post('poll_name'),
        'poll_type_id' => post('poll_type_id')
    ], "`polls`.`poll_id`='{$poll_id}'");

    if ($qr) {
        setAlert('success', "แก้ไขแบบสำรวจสำเร็จเรียบร้อย");
    } else {
        setAlert('error', "เกิดข้อผิดพลาด ไม่สามารถแก้ไขแบบสำรวจได้");
    }

    redirect($page_path);
} elseif (post('q_name')) {
    $qr = DB::insert('questions', [
        'poll_id' => $poll_id,
        'q_name' => post('q_name')
    ]);

    if ($qr) {
        $q_id = DB::$conn->insert_id;
        redirect("/user/polls/edit-question.php?poll={$poll_id}&q=$q_id");
        setAlert('success', "เพื่อคำถามสำเร็จเรียบร้อย");
    } else {
        setAlert('error', "เกิดข้อผิดพลาด ไม่สามารถเพื่อคำถามได้");
    }
}

$data = DB::row("SELECT * FROM `polls` WHERE `poll_id`='{$poll_id}'");
$items = DB::result("SELECT * FROM `questions` WHERE `poll_id`='{$poll_id}'");

$poll_types = DB::result("SELECT * FROM `poll_types`");

ob_start();
?>
<a href="<?= url("/user/polls/list.php") ?>">
    <button>
        < กลับ
    </button>
</a>
<?= showAlert() ?>
<h3>แก้ไขแบบสำรวจ</h3>
<form method="post">
    <label for="poll_name">ชื่อแบบสำรวจ</label>
    <input type="text" name="poll_name" id="poll_name" value="<?= $data['poll_name'] ?>" required>
    <br>
    <label for="poll_type_id">ประเภทแบบสำรวจ</label>
    <select name="poll_type_id" id="poll_type_id">
        <option value="" selected disabled>---- เลือก ----</option>
        <?php foreach ($poll_types as $item) : ?>
            <option value="<?= $item['poll_type_id'] ?>" <?= $item['poll_type_id'] === $data['poll_type_id'] ? 'selected' : '' ?>><?= $item['poll_type_name'] ?></option>
        <?php endforeach; ?>
    </select>
    <br>
    <button type="submit">บันทึก</button>
</form>

<h3>เพื่อคำถาม</h3>
<form method="post">
    <label for="q_name">คำถาม</label>
    <input type="text" name="q_name" id="q_name" required>
    <br>
    <button type="submit">บันทึก</button>
</form>

<h3>รายการคำถาม</h3>
<table>
    <thead>
        <th>รหัส</th>
        <th>คำถาม</th>
        <th>จัดการคำถาม</th>
    </thead>
    <tbody>
        <?php foreach ($items as $item) : ?>
            <tr>
                <td><?= $item['q_id'] ?></td>
                <td><?= $item['q_name'] ?></td>
                <td>
                    <a href="<?= url("/user/polls/edit-question.php?poll={$item['poll_id']}&q={$item['q_id']}") ?>">
                        แก้ไข
                    </a>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="<?= url($page_path) ?>&action=delete&id=<?= $item['q_id'] ?>" <?= clickConfirm("คุณต้องการลบคำถาม {$item['q_name']} หรือไม่") ?>>
                        ลบ
                    </a>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<?php
$layout_page = ob_get_clean();
$page_name = 'แก้ไขแบบสำรวจ';
require ROOT . '/user/layout.php';
