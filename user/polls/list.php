<?php
require_once __DIR__ . '/../../boot.php';
checkAuth('user');

$page_path = "/user/polls/list.php";

$action = get('action');
$id = get('id');
$sql = null;

switch ($action) {
    case 'delete':
        DB::delete('polls', "`poll_id`='{$id}'");
        break;
}

if (isset($action)) {
    redirect($page_path);
}

if ($_POST) {
    $qr = DB::insert('polls', [
        'user_id' => $user_id,
        'poll_name' => post('poll_name'),
        'poll_type_id' => post('poll_type_id')
    ]);

    if ($qr) {
        $poll_id = DB::$conn->insert_id;
        redirect("/user/polls/edit.php?poll={$poll_id}");

        setAlert('success', "เพิ่มแบบสำรวจสำเร็จเรียบร้อย");
    } else {
        setAlert('error', "เกิดข้อผิดพลาด ไม่สามารถเพิ่มแบบสำรวจได้");
    }

    redirect($page_path);
}

$items = DB::result("SELECT * FROM `polls`
LEFT JOIN `poll_types` ON `poll_types`.`poll_type_id`=`polls`.`poll_type_id`
WHERE `polls`.`user_id`='{$user_id}'");

$poll_types = DB::result("SELECT * FROM `poll_types`");

ob_start();
?>
<?= showAlert() ?>
<h3>เพิ่มแบบสำรวจ</h3>
<form method="post">
    <label for="poll_name">ชื่อแบบสำรวจ</label>
    <input type="text" name="poll_name" id="poll_name" required>
    <br>
    <label for="poll_type_id">ประเภทแบบสำรวจ</label>
    <select name="poll_type_id" id="poll_type_id" required>
        <option value="" selected disabled>---- เลือก ----</option>
        <?php foreach ($poll_types as $item) : ?>
            <option value="<?= $item['poll_type_id'] ?>"><?= $item['poll_type_name'] ?></option>
        <?php endforeach; ?>
    </select>
    <br>
    <button type="submit">เพิ่ม</button>
</form>

<h3>รายการแบบสำรวจ</h3>
<table>
    <thead>
        <th>รหัส</th>
        <th>ชื่อแบบสำรวจ</th>
        <th>ชื่อประเภทแบบสำรวจ</th>
        <th>จัดการแบบสำรวจ</th>
        <th></th>
    </thead>
    <tbody>
        <?php foreach ($items as $item) : ?>
            <tr>
                <td><?= $item['poll_id'] ?></td>
                <td><?= $item['poll_name'] ?></td>
                <td><?= $item['poll_type_name'] ?></td>
                <td>
                    <a href="<?= url("/user/polls/edit.php?poll={$item['poll_id']}") ?>">
                        แก้ไข
                    </a>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="?action=delete&id=<?= $item['poll_id'] ?>" <?= clickConfirm("คุณต้องการลบแบบสำรวจ {$item['poll_name']} หรือไม่") ?>>
                        ลบ
                    </a>
                </td>
                <td>
                    <a href="<?= url("/guest/poll.php?id={$item['poll_id']}") ?>" target="_blank" rel="noopener noreferrer">เปิดแบบสำรวจ</a>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<?php
$layout_page = ob_get_clean();
$page_name = 'จัดการแบบสำรวจ';
require ROOT . '/user/layout.php';
